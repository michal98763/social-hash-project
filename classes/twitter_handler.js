var Twitter = require('twitter');
var env = require('../env');


var twitter_handler = {
    client: new Twitter({
        consumer_key: env.twitter_consumer_key,
        consumer_secret: env.twitter_consumer_secret,
        access_token_key: env.twitter_access_token_key,
        access_token_secret: env.twitter_access_token_secret
    }),
    getTwittersPromiseByHash: function (hash) {
        return this.client.get('search/tweets', {q: "#" + hash});
    },
    getTweetParamsForTemplate: function (tweet) {
        var params = {
            tweet: tweet
        };

        return params;
    }
};

module.exports = twitter_handler;