var env = {
    twitter_consumer_key: '',
    twitter_consumer_secret: '',
    twitter_access_token_key: '',
    twitter_access_token_secret: ''
};

module.exports = env;