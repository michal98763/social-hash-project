# Social Hash Project #

Display random tweets by given hashtag. 

### How to start? ###
1. Install nodejs
2. Clone the project
3. Type 'npm install' to install all required dependencies
4. Create 'env.js' file (based on 'env.example.js') and fill in with the Twitter Api Oauth data
5. Type 'npm start' to run the application
6. Run 'http://localhost:3000'
