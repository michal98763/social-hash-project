$(document).ready(function () {

    Index.init();


});

var Index = {
    timeout: 5000,
    currentInterval: null,
    currentHash: null,
    init: function () {
        this.bindEnterClick();
    },
    bindEnterClick: function() {
        $(document).keypress(function(e){
            if (e.which === 13){
                Index.sendHash();
            }
        });
    },
    sendHash :function() {
        if (Index.isHashValid()) {
            Index.startHashTimeout(Index.getHash());
        }
    },
    isHashValid: function () {
        return this.getHash().trim().length > 0;
    },
    getHash: function () {
        return $("#hash-input").val();
    },
    startHashTimeout(hash) {
        if (this.currentHash === hash) {
            return;
        }
        this.currentHash = hash;
        if (this.currentInterval !== null) {
            clearInterval(this.currentInterval);
        }
        this.getTweetByHash();
        this.currentInterval = setInterval(function () {
            Index.getTweetByHash();
        }, Index.timeout);
    },
    getTweetByHash: function () {
        var url = "socials/" + this.currentHash;
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                Index.displayTweet(response);
            },
            error: function (response) {
                var err = $("<h1>Unexpected error!</h1>");
                $("#hashtag").append(err);
            }
        });
    },
    displayTweet: function (tweet) {
        $("#hashtag").text("#" + this.currentHash);
        $("#tweet").empty().append(tweet);
    }

};