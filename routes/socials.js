var express = require('express');
var router = express.Router();
var twitter_handler = require('../classes/twitter_handler');

router.get('/:hash', function (req, res, next) {

    twitter_handler.getTwittersPromiseByHash(req.params.hash)
            .then(function (tweets) {
                var tweetIndex = Math.floor((Math.random() * tweets.statuses.length));
                var tweet = tweets.statuses[tweetIndex];
                res.render('tweet', twitter_handler.getTweetParamsForTemplate(tweet));
            })
            .catch(function (error) {
                res.render('error_tweet');
            });

});


module.exports = router;